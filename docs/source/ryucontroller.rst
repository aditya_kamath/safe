Ryu Based Controller
=============================

This module implements a controller, based on the
`Ryu <http://osrg.github.io/ryu/>`_ framework. The controller uses a slice based
mechanism to ensure that only authenticated hosts are allowed to access network
resources.

=============================


.. autoclass:: main.Switch
   :members:
   :special-members:

.. autoclass:: main.SDNProject
   :members:
   :special-members:
   :private-members: