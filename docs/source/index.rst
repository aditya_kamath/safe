.. SDN-1 Project documentation master file, created by
   sphinx-quickstart on Tue Mar 04 18:35:39 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SDN-1 Project's documentation!
=========================================

This project attempts to implement reliable and secure authentication for hosts
using SDN controllers.

Contents:

.. toctree::
   :maxdepth: 2
   
   authenticator
   ryucontroller



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

