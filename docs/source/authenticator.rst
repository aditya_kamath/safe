Authentication Helper
=============================

This module implements a HTTP request handler that in combination with python's
:class:`BaseHTTPServer.BaseHTTPRequestHandler` authorizes hosts based on a
username/password pair. The usernames and passwords are currently stored in a
python dict. The code could be easily modified to use a database, however

=============================


.. autoclass:: serv.AuthHTTPRequestHandler
   :members:
   :special-members:
   